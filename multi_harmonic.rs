use std::thread;
static LIMIT: u32 = 1000000000;
static NUMBER_OF_THREADS: u32 = 32;

fn main() {
    // Running total of each thread's progress
    let mut total : f64 = 0.0;

    // Array of spawned threads
    let mut children = vec![];

    // Spin up all the threads, point them to their slice of the work
    for x in 1..NUMBER_OF_THREADS + 1 {
        children.push(thread::spawn(move || work(x)));
    }

    // Wait for threads to finish, collect their work, then print
    for child in children {
        total = total + child.join().unwrap();
    }
    println!("{}", total);
}

// Each thread has it's own running total, and computes
// 1/n for every nth number, relative to NUMBER_OF_THREADS
fn work(mut id: u32) -> f64 {
    let mut this_threads_count: f64 = 0.0;
    while id < LIMIT {
        this_threads_count += 1.0 / (id as f64);
        id += NUMBER_OF_THREADS;
    }
    return this_threads_count;
}
