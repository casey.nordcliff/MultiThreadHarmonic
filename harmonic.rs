static LIMIT: u32 = 1000000000;

fn main() {
    let mut sum: f64 = 0.0;
    let mut counter: u32 = 0;
    let mut i: f64 = 1.0;
    while counter < LIMIT {
        sum += 1.0 / i;
        i += 1.0;
        counter += 1;
    }
    println!("{}", sum);
}