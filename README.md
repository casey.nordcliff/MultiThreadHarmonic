Multi-threaded Work Processing in Rust
====================================

This is a simple implementation/example of the differences in performance that we see for a naive vs. multi-threaded
application. These both do the same thing, simply add up the first 1,000,000,000 terms of the harmonic series ``` 1/n ```.
This means it will do ``` 1/1 + 1/2 + 1/3 + ...  + 1/1,000,000,000 ``` and report the result back. The naive implementation
throws all that on a single thread. The multithreaded one distributes the work evenly across however many cores you have.

Assuming you have `rustc` in your $PATH, compile with:

`./compile.sh`

Run with:

`time ./multi && time ./naive`
___
